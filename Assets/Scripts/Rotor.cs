using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotor : MonoBehaviour
{
	[SerializeField] private float Speed;
	private void Update()
    {
         transform.Rotate (Speed * Time.deltaTime,0,0);
    }
}
